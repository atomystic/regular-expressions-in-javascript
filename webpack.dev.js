const commonConfig = require("./webpack.common.js");
const { merge } = require("webpack-merge");
const path = require("path");
module.exports = merge(commonConfig, {
  mode: "development",
  devServer: {
    devMiddleware: {
      writeToDisk: true,
    },
  },
});
