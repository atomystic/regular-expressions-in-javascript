import stateFactory from "../utils/StateFactory.js";
import Time from "../utils/Time.js";
let instance = null;

export default class Experience {
  constructor(resources) {
    // Singleton
    if (instance) {
      return instance;
    }
    instance = this;

    // Variables
    this.resources = resources;

    // this.audio = new Audio(this.resources, true);
    this.time = new Time();
    this.state = stateFactory(location.pathname);
    this.#observerPreparation();
  }

  #observerPreparation() {
    this.time.on("tick", () => this.#update());

    document
      .querySelector(".home__soundIcon__media")
      .addEventListener("click", () => {
        let button = this.resources.audio.find((a) => a.name === "button");
        button.value.volume = button.volume;
        button.value.play();
      });
    document
      .querySelector(".home__soundIcon__media")
      .addEventListener("mouseover", () => {
        let button = this.resources.audio.find((a) => a.name === "buttonHover");
        console.log(button);
        button.value.volume = button.volume;
        button.value.play();
      });
  }

  #update() {
    this.state.update();
  }
}
