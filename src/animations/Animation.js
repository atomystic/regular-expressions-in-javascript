import gsap from "gsap";

export let disappearingWeavyScaleDownAnimation = (
  targetDomElement,
  filterDomElement,
  onComplete,
) => {
  let t1 = gsap.timeline({
    onComplete: onComplete,
  });
  t1.to(
    filterDomElement.baseFrequencyX,
    {
      duration: 2,
      baseVal: 0.005,
      ease: "circ.out",
    },
    0,
  );

  t1.to(
    filterDomElement.baseFrequencyY,
    {
      duration: 2,
      baseVal: 0.015,
      ease: "circ.out",
    },
    0,
  );
  t1.to(
    targetDomElement,
    {
      duration: 2,
      opacity: 0,
      ease: "circ.out",
    },
    0,
  );
  t1.to(
    targetDomElement,
    {
      duration: 2,
      transform: `scale(55%)`,
      ease: "circ.out",
    },
    0,
  );
};

export let disappearingSound = (sound) => {
  let t1 = gsap.timeline();
  t1.to(
    sound,
    {
      duration: 2,
      volume: 0,
      ease: "circ.out",
    },
    0,
  );
};
