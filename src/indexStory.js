import * as THREE from "three";
import wrapDomElementWithTag from "./utils/DomUtils.js";

let homeStoriesStorysDomElement = document.querySelectorAll(
  ".home__stories__story",
);

const homeStoryDomElement = homeStoriesStorysDomElement[0];

wrapDomElementWithTag(
  homeStoryDomElement,
  "span",
  " ",
  "home__stories__story__piece",
);

let story = [];
let stories = [];
let storiesTimeBetween = [];

// Récupérer la liste des time
fetch("intro/story.json").then(async (r) => {
  stories = await r.json();
  story = stories.story[0].words;
});

//Time
let clock = new THREE.Clock();
let currenTime = 0;
let previousTime = 0;
let tick = () => {
  currenTime = clock.getElapsedTime() * 1000;

  storiesTimeBetween = story.forEach((s, i) => {
    if (previousTime <= s.time && s.time < currenTime) {
      homeStoryDomElement.childNodes[i].classList.add(
        "home__stories__story__piece--active",
      );
    }
  });

  // Mettre un stop à l'algo à la fin pour ne pas faire des calcul pour rien
  previousTime = currenTime;
  window.requestAnimationFrame(tick);
};

window.requestAnimationFrame(tick);
