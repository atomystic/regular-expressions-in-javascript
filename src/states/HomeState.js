import wrapDomElementWithTag from "../utils/DomUtils.js";
import Experience from "../experience/Experience.js";
import {
  disappearingWeavyScaleDownAnimation,
  disappearingSound,
} from "../animations/Animation.js";
import Audio from "../classes/Audio.js";

export default class HomeState {
  constructor() {
    // Experience
    this.experience = new Experience();

    // function to no if we are in text animation
    this.isStoryInAnimation = this.#isStoryAlreadyInAnimationBuilder();

    // get all stories
    this.homeStoriesStorysDomElements = this.#getHomeStoriesStorysDomElements();

    //get scroll icon
    this.scrollIcon = document.querySelector(".home__scrollIcon__wrapper");

    this.count = 0;

    //get progressBar
    // this.progress = document.querySelector(".home__progress__level");

    // this.count++;
    // this.progress.style.height = `${(this.count / 5) * 100}%`;

    // get generator of story, voice and words
    this.storyGenerator = this.#createStoriesElementFlow();
    this.storyGeneratorVoice = this.#createStoriesVoiceFlow();
    this.createWordGenerator = this.#createStoriesWordFlow();
    this.createDurationGenerator = this.#createDurationWordFlow();

    // step generator
    this.forward = this.#nextStepGenerator();
    this.currentStory = this.storyGenerator.next();
    this.currentStoryVoice = this.storyGeneratorVoice.next();
    this.currentStoryWords = this.createWordGenerator.next();
    this.currentDuration = this.createDurationGenerator.next();

    this.getTurbulenceFilterElement;

    // initiation for the proper functioning of the intro
    this.#getNPlayBackgroundMusic();
    this.#htmlElementPreparation();
    this.#subscribeForwardStoryAnimation();
  }

  #getNPlayBackgroundMusic() {
    this.audio = this.experience.resources.audio.find(
      (a) => a.name === "KevinMacLeod-HeavyHeart",
    );
    this.backgroundMusic = new Audio(this.audio, {
      volume: this.audio.volume,
      isLoop: true,
    });
    this.backgroundMusic.play();
  }
  #getHomeStoriesStorysDomElements() {
    return document.querySelectorAll(".home__stories__story");
  }

  #htmlElementPreparation() {
    this.getTurbulenceFilterElement = document.querySelector(
      ".home__stories__filter__turbulence",
    );
    // bring up the first story
    this.currentStory.value.classList.add("home__stories__story--active");

    // spannified all word in all story
    this.homeStoriesStorysDomElements.forEach((el) => {
      wrapDomElementWithTag(el, "span", " ", "home__stories__story__piece");
    });
  }

  #subscribeForwardStoryAnimation() {
    document.addEventListener("wheel", (e) => {
      if (
        !this.isStoryInAnimation.isStoryAlreadyInAnimation() &&
        e.deltaY > 0
      ) {
        this.scrollIcon.style.opacity = 0;
        this.forward.next();
      }
    });

    document.addEventListener("keyup", (e) => {
      if (
        !this.isStoryInAnimation.isStoryAlreadyInAnimation() &&
        (e.key === "ArrowDown" || e.key.toLowerCase() === "s")
      ) {
        this.forward.next();
      }
    });
    this.scrollIcon.addEventListener("click", () => {
      this.forward.next();
    });

    // TODO Appelé en cliquant sur l'icône de scroll
    // TODO Appelé AU SWIPE sur mobile
  }

  updateInterfaceAndRelaunchTimer = () => {
    this.currentStory.value.classList.remove("home__stories__story--active");

    this.currentStoryVoice.value.pause();
    this.currentStoryVoice = null;

    this.currentStory = this.storyGenerator.next();
    this.currentStoryVoice = this.storyGeneratorVoice.next();
    this.currentStoryWords = this.createWordGenerator.next();
    this.currentDuration = this.createDurationGenerator.next();

    if (this.currentStory.done === true) {
      return;
    }

    this.currentStory.value.classList.add("home__stories__story--active");
    this.currentStoryVoice.value.play();

    this.getTurbulenceFilterElement.baseFrequencyX.baseVal = 0;
    this.getTurbulenceFilterElement.baseFrequencyY.baseVal = 0;

    // reset and relaunch the Timer
    this.experience.time.stopAndReset();
    this.experience.time.startAfterReset();
  };

  *#nextStepGenerator() {
    this.scrollIcon.style.opacity = 0;
    for (let i = 0; i < this.homeStoriesStorysDomElements.length - 1; i++) {
      // this.count++;
      // this.progress.style.height = `${(this.count / 5) * 100}%`;
      this.isStoryInAnimation.setIsInAnimation();
      disappearingSound(this.currentStoryVoice.value.audio);
      disappearingWeavyScaleDownAnimation(
        this.currentStory.value,
        this.getTurbulenceFilterElement,
        this.updateInterfaceAndRelaunchTimer.bind(this),
      );
      yield;
    }
    // se déroule soit au scroll, soit à la fin du temps du timer
    disappearingWeavyScaleDownAnimation(
      this.currentStory.value,
      this.getTurbulenceFilterElement,
      this.updateInterfaceAndRelaunchTimer.bind(this),
    );
    yield;
    disappearingWeavyScaleDownAnimation(
      this.currentStory.value,
      this.getTurbulenceFilterElement,
      this.updateInterfaceAndRelaunchTimer.bind(this),
    );
  }

  *#createStoriesElementFlow() {
    let count = 0;
    while (true) {
      if (count < this.homeStoriesStorysDomElements.length) {
        yield this.homeStoriesStorysDomElements[count];
        count++;
      } else break;
    }
  }
  *#createStoriesVoiceFlow() {
    let count = 0;
    while (true) {
      if (count < this.experience.resources.story.length) {
        let a = this.experience.resources.story[count].audio[0];
        let audio = new Audio(a, { volume: a.volume });
        audio.play();
        yield audio;
        count++;
      } else break;
    }
  }
  *#createStoriesWordFlow() {
    let count = 0;
    while (true) {
      if (count < this.experience.resources.story.length) {
        yield this.experience.resources.story[count].words;
        count++;
      } else break;
    }
  }
  *#createDurationWordFlow() {
    let count = 0;
    while (true) {
      if (count < this.experience.resources.story.length) {
        yield this.experience.resources.story[count].duration;
        count++;
      } else break;
    }
  }
  #isStoryAlreadyInAnimationBuilder() {
    let isStoryAbleToBeInAnimation = false;
    let setIsInAnimation = () => {
      isStoryAbleToBeInAnimation = true;
      setTimeout(() => {
        isStoryAbleToBeInAnimation = false;
      }, 3000);
    };
    let isStoryAlreadyInAnimation = () => isStoryAbleToBeInAnimation;
    return { setIsInAnimation, isStoryAlreadyInAnimation };
  }

  update() {
    this.currentStoryWords.value.forEach((s, i) => {
      if (
        this.experience.time.previousTime <= s.time &&
        s.time < this.experience.time.currentTime &&
        (!this.currentStory.done || !this.currentStory.value === undefined)
      ) {
        this.currentStory.value.childNodes[i].classList.add(
          "home__stories__story__piece--active",
        );
      }
    });
    if (
      this.experience.time.previousTime <= this.currentDuration.value &&
      this.currentDuration.value < this.experience.time.currentTime
    ) {
      this.scrollIcon.style.opacity = 1;
    }
  }
}

// faire en sorte de mettre
