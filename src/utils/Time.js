import EventEmitter from "./EventEmitter.js";
import * as THREE from "three";
export default class Time extends EventEmitter {
  constructor() {
    super();
    this.clock = new THREE.Clock();
    this.currentTime = 16;
    this.previousTime = 0;
    this.play = true;
    this.tick();
  }

  reset() {
    this.currentTime = 16;
    this.previousTime = 0;
  }

  pause() {
    this.play = false;
    this.clock.stop();
  }

  start() {
    this.clock.start();
    this.play = true;
  }

  startAfterReset() {
    this.clock = new THREE.Clock();
    this.play = true;
  }

  stopAndReset() {
    this.pause();
    this.reset();
  }

  tick() {
    if (this.play) {
      this.currentTime = this.clock.getElapsedTime() * 1000;
      this.trigger("tick");
      this.previousTime = this.currentTime;
    }
    window.requestAnimationFrame(() => this.tick());
  }
}
