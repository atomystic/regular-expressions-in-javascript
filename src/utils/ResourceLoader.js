import resourcePathFactory from "./ResourcePathFactory.js";
import EventEmitter from "./EventEmitter.js";

export default class ResourceLoader extends EventEmitter {
  constructor() {
    super();
    this.numberOfResources;
    this.resources;
    this.numberOfResourcesLoaded = 0;
    this.percentage = "0%";
  }

  async load(currentUrl) {
    const response = await fetch(resourcePathFactory(currentUrl));
    const resources = await response.json();
    this.resources = resources;

    // TODO  changer le compteur de ressource
    this.numberOfResources = Object.entries(resources)
      .map(([key, value]) => value)
      .flat().length;

    await this.traverse(resources);
    return resources;
  }

  getPercentage() {
    return (
      (this.numberOfResourcesLoaded / this.numberOfResources).toFixed(0) * 100
    );
  }
  getRatio() {
    return this.numberOfResourcesLoaded / this.numberOfResources;
  }

  async *audioGenerator(audioElements) {
    for (const audio of audioElements) {
      if (audio.toFetch === "false") yield (audio.value = new Audio(audio.url));
      else {
        audio.value = await fetch(audio.url);
        yield audio.value;
      }
    }
  }

  async traverse(resources) {
    Object.entries(resources).forEach(async ([key, value]) => {
      if (key === "audio") {
        for await (const audio of this.audioGenerator(value)) {
        }
      } else if (value instanceof Array) {
        value.forEach((v) => this.traverse(v));
      }
    });
  }
}
