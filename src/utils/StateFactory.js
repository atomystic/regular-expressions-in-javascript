import HomeState from "../states/HomeState.js";
const stateFactory = (urlPath) => {
  switch (urlPath) {
    default:
      return new HomeState();
  }
};
export default stateFactory;
