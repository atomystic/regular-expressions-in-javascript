const wrapDomElementWithTag = (domElement, tag, separator, classToAdd) => {
  // Retrieving the textual content of the active element
  const domElementText = domElement.textContent;

  // Put each word in a table
  const domElementTab = domElementText.split(separator);

  // Browse the array of words to create spans around
  let wraps = domElementTab.map((w, i, t) => {
    const tagElement = document.createElement(tag);
    tagElement.classList.add(classToAdd);
    let tagElementText = `${w}`;
    if (i < t.length - 1) {
      tagElementText += " ";
    }
    tagElement.textContent = tagElementText;
    if ("“Expressions régulières”.".indexOf(w) >= 0) {
      tagElement.classList.add("home__stories__story__piece--golden");
    }
    return tagElement;
  });

  // Injection of the spans in the dom
  domElement.textContent = "";
  wraps.forEach((w) => domElement.appendChild(w));
};

export default wrapDomElementWithTag;
