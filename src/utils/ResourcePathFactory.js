const resourcePathFactory = (urlPath) => {
  switch (urlPath) {
    default:
      return "home.json";
  }
};
export default resourcePathFactory;
