import Experience from "./experience/Experience.js";
import gsap from "gsap";
import ResourceLoader from "./utils/ResourceLoader.js";

const loader = new ResourceLoader();
const buttonBeginExperience = document.querySelector(
  ".preloader__button__wrapper",
);
const preload = document.querySelector(".preloader__wrapper");
const button = document.querySelector(".preloader__button__wrapper");
const preloader = document.querySelector(".preloader");

loader.on("change", () => {
  gsap.to(".preloader__path", {
    duration: 1.5,
    width: `${(1 - loader.getRatio()) * 81}rem)`,
    delay: 0,
  });
  document.querySelector(
    ".preloader__percentage",
  ).textContent = `${loader.getPercentage()}%`;
});

loader.load(location.pathname).then((resources) => {
  setTimeout(() => {
    buttonBeginExperience.style.opacity = "1";
    preload.style.opacity = "0";
  }, 1500);
  button.addEventListener("click", () => {
    let t1 = gsap.timeline({
      onComplete: () => {
        preloader.style.display = "none";
      },
    });
    t1.to(preloader, {
      duration: 1.5,
      opacity: 0,
      delay: 0,
    });
    setTimeout(() => new Experience(resources), 4000);
  });
});
// supprimer le listener
