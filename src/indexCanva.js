import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import * as dat from "dat.gui";

import magicPowerVert from "./shader/circleMagicShader/CircleMagicVert.glsl";
import magicPowerFrag from "./shader/circleMagicShader/CircleMagicFrag.glsl";

import SphereMagicVert from "./shader/sphereMagicShader/SphereMagicVert.glsl";
import ShpereMagicFrag from "./shader/sphereMagicShader/ShpereMagicFrag.glsl";

import { UnrealBloomPass } from "three/examples/jsm/postprocessing/UnrealBloomPass.js";
import { WebGLRenderTarget } from "three/src/renderers/WebGLRenderTarget.js";
import { EffectComposer } from "three/examples/jsm/postprocessing/EffectComposer.js";
import { RenderPass } from "three/examples/jsm/postprocessing/RenderPass.js";

// Debug
const debugObject = {};
debugObject.ringDarkColor = "#a37f03";
debugObject.ringLightColor = "#ffffff";
debugObject.sphereColor = "#ffe0b1";
const gui = new dat.GUI();

// Canvas
const canvas = document.querySelector("canvas.webgl");

// Scene
const scene = new THREE.Scene();

// Geometry
const ringGeometry = new THREE.TorusGeometry(2, 0.04, 3, 30);
const ringGeometry2 = new THREE.TorusGeometry(2.2, 0.03, 3, 30);
const ringGeometry3 = new THREE.TorusGeometry(2.5, 0.04, 3, 30);
const ringGeometry4 = new THREE.TorusGeometry(2.7, 0.05, 3, 30);
const ringGeometry5 = new THREE.TorusGeometry(3, 0.03, 3, 30);
const sphereGeometry = new THREE.SphereGeometry(0.3, 32, 16);

// pour la mise en place de particule
// buffer geometry
// création d'un tableau de position
// mise du tableau dans le buffer
// ajout du shader material au buffer
// construction de la mesh
// ajout de la mesh à la scène

// Material
const material = new THREE.ShaderMaterial({
  side: THREE.DoubleSide,
  vertexShader: magicPowerVert,
  fragmentShader: magicPowerFrag,
  depthWrite: false,
  blending: THREE.AdditiveBlending,
  vertexColors: true,
  uniforms: {
    ringDarkColor: { value: new THREE.Color(debugObject.ringDarkColor) },
    ringLightColor: { value: new THREE.Color(debugObject.ringLightColor) },
    uTime: { value: 0 },
    lightVelocity: { value: 7 },
  },
});

const sphereMaterial = new THREE.ShaderMaterial({
  side: THREE.DoubleSide,
  vertexShader: SphereMagicVert,
  fragmentShader: ShpereMagicFrag,
  depthWrite: false,
  blending: THREE.AdditiveBlending,
  uniforms: {
    sphereColor: { value: new THREE.Color(debugObject.sphereColor) },
  },
});

// Mesh
const ringMeshes = [];
const mesh = new THREE.Mesh(ringGeometry, material);
const mesh2 = new THREE.Mesh(ringGeometry2, material);
mesh2.rotation.x = Math.random() * 2 * Math.PI;
mesh2.rotation.y = Math.random() * 2 * Math.PI;
mesh2.rotation.z = Math.random() * 2 * Math.PI;
const mesh3 = new THREE.Mesh(ringGeometry3, material);
mesh3.rotation.x = Math.random() * 2 * Math.PI;
mesh3.rotation.y = Math.random() * 2 * Math.PI;
mesh3.rotation.z = Math.random() * 2 * Math.PI;
const mesh4 = new THREE.Mesh(ringGeometry4, material);
mesh4.rotation.x = Math.random() * 2 * Math.PI;
mesh4.rotation.y = Math.random() * 2 * Math.PI;
mesh4.rotation.z = Math.random() * 2 * Math.PI;
const mesh5 = new THREE.Mesh(ringGeometry5, material);
mesh5.rotation.x = Math.random() * 2 * Math.PI;
mesh5.rotation.y = Math.random() * 2 * Math.PI;
mesh5.rotation.z = Math.random() * 2 * Math.PI;
const mesh6 = new THREE.Mesh(sphereGeometry, sphereMaterial);
mesh6.position.set(Math.random(), Math.random(), Math.random());
// scene.add(mesh6);

const sphereMesh = new THREE.Mesh(sphereGeometry, sphereMaterial);
sphereMesh.position.z = -25;
// scene.add(sphereMesh);
// scene.add(mesh);
ringMeshes.push(mesh);
// scene.add(mesh2);
ringMeshes.push(mesh2);
// scene.add(mesh3);
ringMeshes.push(mesh3);
// scene.add(mesh4);
ringMeshes.push(mesh4);
// scene.add(mesh5);
ringMeshes.push(mesh5);
ringMeshes.forEach((e) => {
  e.position.z = -25;
});

// gui
gui
  .addColor(debugObject, "ringDarkColor")
  .name("ringDarkColor")
  .onChange(() => {
    material.uniforms.ringDarkColor.value = new THREE.Color(
      debugObject.ringDarkColor,
    );
  });
gui
  .addColor(debugObject, "ringLightColor")
  .name("ringLightColor")
  .onChange(() => {
    material.uniforms.ringLightColor.value = new THREE.Color(
      debugObject.ringLightColor,
    );
  });

gui
  .addColor(debugObject, "sphereColor")
  .name("Sphere color")
  .onChange(() => {
    sphereMaterial.uniforms.sphereColor.value = new THREE.Color(
      debugObject.sphereColor,
    );
  });

// Sizes
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};

window.addEventListener("resize", () => {
  // Update sizes
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;

  // Update camera
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  // Update renderer
  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

// Camera
const camera = new THREE.PerspectiveCamera(
  65,
  sizes.width / sizes.height,
  0.1,
  100,
);
camera.position.set(0, 0, 15);
scene.add(camera);

// Controls
// const controls = new OrbitControls(camera, canvas);
// controls.enableDamping = true;

// Renderer
const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
});

// RenderTarget
const options = {};
options.minFilter = THREE.LinearFilter;
options.magFilter = THREE.LinearFilter;
options.format = THREE.RGBAFormat;
options.encoding = THREE.sRGBEncoding;
const renderTarget = new WebGLRenderTarget(sizes.width, sizes.height, options);
renderTarget.samples = 1;

// Composer
const effectComposer = new EffectComposer(renderer, renderTarget);
effectComposer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
effectComposer.setSize(sizes.width, sizes.height);
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));

// Passes
const renderPass = new RenderPass(scene, camera);
effectComposer.addPass(renderPass);

// UnrealBloomPass
const unrealBloomPass = new UnrealBloomPass();
unrealBloomPass.enabled = true;
unrealBloomPass.strength = 1.2;
unrealBloomPass.radius = 0;
unrealBloomPass.threshold = 0;
effectComposer.addPass(unrealBloomPass);

gui.add(unrealBloomPass, "enabled");
gui.add(unrealBloomPass, "strength").min(0).max(2).step(0.001);
gui.add(unrealBloomPass, "radius").min(0).max(2).step(0.001);
gui.add(unrealBloomPass, "threshold").min(0).max(1).step(0.001);

// Clock
let clock = new THREE.Clock();
let currenTime = 0;
let deltaTime = 0;
let previousTime = 0;

const lerp = (a, b, c, d, e) => {
  const f = d + ((b - a) / (c - a)) * (e - d);
  return f;
};

//Raycaster
// différence de position camera object
const diff = -10;
window.addEventListener("mousemove", (e) => {
  // conversion pixel en valeur entre -1 et 1;
  const x = (e.clientX / sizes.width) * 2.0 - 1.0;
  const y = (-e.clientY / sizes.height) * 2.0 + 1.0;
  const xx = Math.atan(0.5 * camera.fov) * (sizes.width / 1920) * 40.0;
  const xxx = 0.855 * xx;
  const posX = lerp(-1, x, 1, -xxx, xxx);
  sphereMesh.position.x = posX;
  ringMeshes.forEach((e) => {
    e.position.x = posX;
  });
  const yy =
    Math.atan(0.5 * camera.fov) * (sizes.height / 1080) * (1080 / 1920) * 20.0;
  const yyy = 0.955 * yy;
  const posY = lerp(-1, y, 1, -yyy, yyy);
  sphereMesh.position.y = posY;
  ringMeshes.forEach((e) => {
    e.position.y = posY;
    console.log(e.position.unproject);
  });
});
// délimiter les bordures

let velocityX = 0;
let velocityY = 0;
let velocityZ = 0;
const speed = 0;
let tick = () => {
  // controls.update();
  // renderer.render(scene, camera);
  currenTime = clock.getElapsedTime();
  deltaTime = currenTime - previousTime;
  ringMeshes.forEach((e) => {
    e.rotation.x += deltaTime * 1.9;
    e.rotation.y += deltaTime * 0.9;
    e.rotation.z += deltaTime * 1.3;
  });
  material.uniforms.uTime.value = currenTime;

  const speed = deltaTime * 0.25;

  //accélération
  // velocityY += speed * (sphereMesh.position.y - mesh6.position.y);
  // velocityX += speed * (sphereMesh.position.x - mesh6.position.x);
  // velocityZ += speed * (sphereMesh.position.z - mesh6.position.z);

  velocityY += speed * (sphereMesh.position.y - mesh6.position.y);
  velocityX += speed * (sphereMesh.position.x - mesh6.position.x);
  velocityZ += speed * (sphereMesh.position.z - mesh6.position.z);

  velocityY *= 0.99;
  velocityX *= 0.99;
  velocityZ *= 0.99;

  mesh6.position.y += velocityY;
  mesh6.position.x += velocityX;
  mesh6.position.z += velocityZ;

  previousTime = currenTime;

  effectComposer.render();
  window.requestAnimationFrame(tick);
};

tick();
