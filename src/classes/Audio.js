export default class Audio {
  constructor(audio, options) {
    this.audio = audio;
    this.options = options;
    if (options) {
      this.audio.value.loop = this.options.isLoop;
      this.audio.value.volume = this.options.volume;
    }
  }

  play() {
    this.audio.value.play();
  }

  pause() {
    this.audio.value.pause();
  }
}
