uniform vec3 ringDarkColor;
uniform vec3 ringLightColor;
uniform float uTime;
uniform float lightVelocity;
varying vec2 vUv;

#define PI 3.1415926538

void main() {

    float xGradient = pow(((cos((vUv.x * PI * 2.0) + uTime * lightVelocity) + 1.0) / 2.0), 10.0);

    vec3 color = mix(ringDarkColor, ringLightColor, xGradient);

    float transparency = pow(xGradient, 0.6);

    gl_FragColor = vec4(color, transparency);
}