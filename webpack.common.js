const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
module.exports = {
  experiments: {
    topLevelAwait: true,
  },
  entry: [
    path.join(__dirname, "src/index.js"),
    path.join(__dirname, "styles/index.scss"),
  ],
  devtool: "inline-source-map",
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "./public"),
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {
          from: "./shared",
          to: "",
        },
        {
          from: "./data",
          to: "",
        },
      ],
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: "babel-loader",
        },
      },
      {
        test: /\.html$/,
        use: ["html-loader"],
      },
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "postcss-loader",
          "sass-loader",
        ],
      },
      {
        test: /\.(png|woff2)$/,
        use: ["file-loader"],
      },
      {
        test: /\.glsl$/i,
        loader: "raw-loader",
        exclude: /node_modules/,
      },
    ],
  },
};
