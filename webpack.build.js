const commonConfig = require("./webpack.common.js");
const { merge } = require("webpack-merge");
module.exports = merge(commonConfig, {
  mode: "production",
  //permet de copier les fichier dans le dossier output
  devServer: {
    devMiddleware: {
      writeToDisk: true,
    },
  },
});
