// fichier d'environnement
require("dotenv").config();
// fonction permettant d'aller chercher les données de l'API Prismic.
const initApi = (req) => {
  return Prismic.getApi(process.env.PRISMIC_ENDPOINT, {
    accessToken: process.env.PRISMIC_ACCESS_TOKEN,
  });
};

const path = require("path");

// Server
const express = require("express");
const port = 3000;
const app = express();
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});

// utiliser le moteur de template pug et la position des pages pug
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

//middleware qui se lance après chaque requête envoyé à un root
app.use((req, res, next) => {
  next();
});
// ajouter d'autre middelware.

// import de prismic
const Prismic = require("prismic-javascript");

// servir les fichiers static du dossier public dans express
app.use(express.static(path.join(__dirname, "public")));

// root
app.get("/", async (req, res) => {
  const api = await initApi(req);
  const meta = await api.getSingle("meta");
  const home = await api.getSingle("home");

  res.render("pages/home", { meta, home });
});
